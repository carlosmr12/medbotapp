# -*- coding: utf-8 -*-
from app import app,db
from datetime import datetime
from flask import request
from flask_mail import Mail, Message
import apiai
import json
import requests

from db_helper import db_helper
from models import Users,Doctors

import config

def send_mail(to_address="",from_address="medbotapp@gmail.com",subject="",message=""):
    """
        Helper function for sending mail through gmail smtp and flask mail
    """
    mail = Mail(app)

    msg = Message(subject,sender=from_address,recipients=[to_address])
    msg.body = message
    mail.send(msg)
    return True

def get_response(user,results_dict):
    """
        Getting response based on results from api.ai processing
    """
    json_data = str()
    GREETING = results_dict.get('greeting',None)
    MEDICINE = results_dict.get('medicine',None)
    SCHEDULING = results_dict.get('scheduling',None)
    SEND_MAIL = results_dict.get('mail_reply',None)
    HELP = results_dict.get('help',None)

    SCHEDULE_DATE = results_dict.get('schedule_date',None)
    SCHEDULE_TIME = results_dict.get('schedule_time',None)
    SCHEDULE_ESP = results_dict.get('schedule_espe',None)
    EMAIL = results_dict.get('email',None)

    THANKS = results_dict.get('thanks',None)
    BYE = results_dict.get('bye',None)
    THANKS_BYE = results_dict.get('thanks_bye',None)

    MESSAGE2REPLY = str()
    if (GREETING is not None and GREETING != '') and (MEDICINE == '' or MEDICINE == None) and (SCHEDULING == '' or SCHEDULING == None):
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Olá {},\neu sou o MedBot, um assistente virtual para ajudar você na busca e no agendamento de consultas médicas.\nPara começar eu gostaria de saber para qual tipo de médico (especialidade) você está procurando atendimento (cardiologista,dermatologista,ortopedista, etc)?".format(user.first_name)}})
        MESSAGE2REPLY = "ask_medicine"
    elif (HELP is not None and HELP != ''):
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Olá {},\neu sou o MedBot, um assistente virtual para ajudar você na busca e no agendamento de consultas médicas.\nPara começar eu gostaria de saber para qual tipo de médico (especialidade) você está procurando atendimento (cardiologista,dermatologista,ortopedista, etc)?".format(user.first_name)}})
        MESSAGE2REPLY = "help"
    elif (GREETING is not None and GREETING != '') and (MEDICINE is not None and MEDICINE != '') and (SCHEDULING is None or SCHEDULING == ''):
        #doctors = db_helper.get_doctors_by_medicine(MEDICINE)
        doctors = db.session.query(Doctors).filter_by(Doctors.medicine.has(name=MEDICINE))
        quick_replies = list()
        for doc in doctors:
            quick_replies.append({"content_type":"text","title":doc.name,"payload":"{} docid".format(doc.id)})
        json_data = json.dumps({"recipient":{"id":user.user_id},"message":{"text":"Olá {},aqui está uma lista com alguns médicos especialistas em {}. Por favor selecione um deles:".format(user.first_name,MEDICINE),"quick_replies":quick_replies}})
        MESSAGE2REPLY = "ask_doctor"
    elif (GREETING is None or GREETING == '') and (MEDICINE is not None and MEDICINE != '') and (SCHEDULING is None or SCHEDULING == ''):
        #doctors = db_helper.get_doctors_by_medicine(MEDICINE)
        doctors = db.session.query(Doctors).filter(Doctors.medicine.has(name=MEDICINE))
        quick_replies = list()
        for doc in doctors:
            quick_replies.append({"content_type":"text","title":doc.name,"payload":"{} docid".format(doc.id)})
        json_data = json.dumps({"recipient":{"id":user.user_id},"message":{"text":"Aqui está uma lista com alguns médicos especialistas em {}. Por favor selecione um deles:".format(MEDICINE),"quick_replies":quick_replies}})
        MESSAGE2REPLY = "ask_doctor"
    elif (GREETING is None or GREETING == '') and (MEDICINE is None or MEDICINE == '') and (SCHEDULING is not None and SCHEDULING != ''):
        #doctor = db_helper.get_doctor_by_id(SCHEDULING['docid'])
        doctor = db.session.query(Doctors).filter_by(id=SCHEDULING['docid']).first()
        json_data = json.dumps({"recipient":{"id":user.user_id},"message":{"text":"Aqui está a lista das datas disponíveis para {}. Escolha uma das opções para verificar os horários disponíveis:".format(doctor.name.encode('utf-8')),"quick_replies":[{"content_type":"text","title":"01/12/2016","payload":"01/12/2016 scheduledate"},{"content_type":"text","title":"02/12/2016","payload":"02/12/2016 scheduledate"},{"content_type":"text","title":"03/12/2016","payload":"03/12/2016 scheduledate"},{"content_type":"text","title":"04/12/2016","payload":"04/12/2016 scheduledate"},{"content_type":"text","title":"05/12/2016","payload":"05/12/2016 scheduledate"}]}})
        MESSAGE2REPLY = "ask_date"
    elif SCHEDULE_DATE is not None:
        schedule_date = datetime.strptime(SCHEDULE_DATE['scheduledate'], '%Y-%m-%d').strftime('%d/%m/%Y')
        json_data = json.dumps({"recipient":{"id":user.user_id},"message":{"text":"Aqui está a lista dos horários disponíveis para o dia {}. Escolha uma das opções para agendar a sua consulta:".format(schedule_date),"quick_replies":[{"content_type":"text","title":"08:00","payload":"08:00 scheduletime"},{"content_type":"text","title":"08:30","payload":"08:30 scheduletime"},{"content_type":"text","title":"10:30","payload":"10:30 scheduletime"},{"content_type":"text","title":"15:00","payload":"15:00 scheduletime"},{"content_type":"text","title":"16:30","payload":"16:30 scheduletime"}]}})
        MESSAGE2REPLY = "ask_time"
    elif SCHEDULE_TIME is not None:
        json_data = json.dumps({"recipient":{"id":user.user_id},"message":{"text":"Sua consulta foi confirmada com sucesso.Você deseja que eu envie um EMAIL com as informações do agendamento?","quick_replies":[{"content_type":"text","title":"Sim","payload":"YES_MAIL"},{"content_type":"text","title":"Não","payload":"NO_MAIL"}]}})
        MESSAGE2REPLY = "ask_email_yes_no"
    elif SEND_MAIL == 'YES_MAIL':
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Para qual EMAIL eu devo enviar essas informações?"}})
        MESSAGE2REPLY = "ask_email"
    elif SEND_MAIL == 'NO_MAIL':
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Fico feliz por ter ajudado, se precisar é só chamar."}})
        MESSAGE2REPLY = "bye"
    elif EMAIL is not None:
        send_mail(to_address=EMAIL,subject="Informações de agendamento",message="Olá {},\nObrigado por utilizar o Medbot. Abaixo estão alguns dados do seu agendamento:\nMédico:\tMEDICO\nData:\tDATA\nHorário:\tHORARIO\nEndereço:END\n\nAbraço,\nMedbot team".format(user.first_name))
        #print "Mail sent"
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Email enviado! Fico feliz por ter ajudado, se precisar é só chamar."}})
        MESSAGE2REPLY = "mail_sent"
    elif THANKS is not None:
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Por nada. Fico feliz por ter ajudado, se precisar é só chamar."}})
        MESSAGE2REPLY = "thanks"
    elif BYE is not None:
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Até a próxima, se precisar é só chamar."}})
        MESSAGE2REPLY = "bye"
    elif THANKS_BYE is not None:
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Por nada! Fico feliz por ter ajudado, se precisar é só chamar. Até a próxima."}})
        MESSAGE2REPLY = "thanks_bye"
    else:
        json_data = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Fico feliz por ter ajudado, se precisar é só chamar."}})
        MESSAGE2REPLY = "bye"

    #db_helper.set_last_message(user.user_id,MESSAGE2REPLY)
    user.last_message = MESSAGE2REPLY
    db.session.commit()

    return json_data

def messaging_events(payload):
    """
        Generate tuples of (sender_id, message_text) from provided payload
    """
    data = json.loads(payload)
    messaging_events = data['entry'][0]['messaging']
    for event in messaging_events:
        if "quick_reply" in event['message']:
            yield event['sender']['id'], event['message']['quick_reply']['payload'],event['message']['seq']
        elif "message" in event and "text" in event['message']:
            # unicode_escape can help us handle emojis that usually break down the text processing
            yield event['sender']['id'],event['message']['text'].encode('unicode_escape'),event['message']['seq']
        else:
            yield event['sender']['id'],"Desculpe, mas eu não entendi muito bem o que você quis dizer.\nDigite AJUDA para começar.",event['message']['seq']

def send_message(token, user, text):
    """
        Send message text to recipient with id recipient
    """
    message2response = str()
    #print "Querying api.ai"
    ai = apiai.ApiAI(config.CLIENT_ACCESS_TOKEN)
    request = ai.text_request()
    request.query = text.decode('unicode_escape')

    response = request.getresponse()

    ai_response = json.loads(response.read())
    results_dict = dict()

    #print "Handling response"
    if ai_response['result']['parameters']:
        results_dict = ai_response['result']['parameters']
        message2response = get_response(user, results_dict)

    else:
        if user.last_message == "ask_medicine":
            message2response = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Desculpe, mas eu não conheço essa especialidade médica. Alguns exemplos de especialidades que eu conheço são cardiologia, dermatologia, urologia e muitas outras. Por favor, tente novamente."}})
        elif user.last_message == "ask_email":
            message2response = json.dumps({'recipient':{'id':user.user_id},'message':{'text':"Desculpe, mas esse não parece ser um email válido. Por favor, tente novamente."}})
        else:
            message2response = json.dumps({'recipient':{'id':user.user_id},'message':{'text':ai_response['result']['fulfillment']['speech']}})

    r = requests.post("https://graph.facebook.com/v2.8/me/messages", params={"access_token": token}, data=message2response, headers={'Content-type':'application/json'})

    if r.status_code != requests.codes.ok:
        print r.text

def get_user(token, user_id):
    """
        Getting user primary information"
    """
    user = db.session.query(Users).filter_by(user_id=user_id).first()
    #user = db_helper.get_user(user_id)
    if user is None:
        #print "Facebook graph api query"
        headers = {'Content-Type':'application/json'}
        params ={'access_token':token,'fields':'first_name,last_name'}
        r = requests.get("https://graph.facebook.com/v2.6/{}".format(user_id),params=params,headers=headers)

        if r.status_code == requests.codes.ok:
            user_info = r.json()
            #user = db_helper.set_user(user_id,user_info['first_name'],user_info['last_name'],"")
            user = Users(user_id=user_id,first_name=user_info['first_name'],last_name=user_info['last_name'],locale='',gender='',last_message='')
            db.session.add(user)
            db.session.commit()
    return user
