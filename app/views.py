# -*- coding: utf-8 -*-
from app import app,db
from flask import request
import json

from db_helper import db_helper
import config
import helpers

from models import ProcessedMessages

@app.route('/', methods=['GET'])
def handle_verification():
    #print "Handling verification"
    mode = request.args.get('hub.mode')
    validation_token = request.args.get('hub.verify_token')
    if mode == 'subscribe' and validation_token == config.VALIDATION_TOKEN:
        challenge = request.args.get('hub.challenge')
        return challenge
    else:
        #print "Verification failed!"
        return 'Error, wrong validation token!'

@app.route('/', methods=['POST'])
def handle_messages():
    #print "Handling messages"
    payload = request.get_data()
    for sender, message, seqid in helpers.messaging_events(payload):
        is_processed = db.session.query(ProcessedMessages).filter_by(seqid=str(seqid)).first()
        if not is_processed:
            message_to_process = ProcessedMessages(seqid=str(seqid))
            db.session.add(message_to_process)
            db.session.commit()
            #db_helper.set_message_processed(seqid)
            user = helpers.get_user(config.PAT,sender)
            #print "Incoming from {}: {}".format(user.user_id, message)
            helpers.send_message(config.PAT,user,message)
    return "ok"
