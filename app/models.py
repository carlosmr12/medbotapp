from app import db

class Users(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.String(255))
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    last_message = db.Column(db.String(1255))
    locale = db.Column(db.String(255))
    gender = db.Column(db.String(10))

    def __init__(self,user_id,first_name,last_name,last_message,locale,gender):
        self.user_id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.last_message = last_message
        self.locale = locale
        self.gender = gender

    def __repr__(self):
        return '<id {}>'.format(self.id)

class Doctors(db.Model):
    __tablename__ = 'doctors'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(255))
    medicine_id = db.Column(db.Integer, db.ForeignKey('medicines.id'))
    medicine = db.relationship("Medicines",backref="parents")
    address = db.Column(db.String(1255))

    def __init__(self,name,medicine_id,address):
        self.name = name
        self.medicine_id = medicine_id
        self.address = address

    def __repr__(self):
        return '<id {}>'.format(self.id)

class Medicines(db.Model):
    __tablename__ = 'medicines'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(255))

    def __init__(self,name):
        self.name = name

    def __repr__(self):
        return '<id {}>'.format(self.id)

class ProcessedMessages(db.Model):
    __tablename__ = 'processed_messages'

    id = db.Column(db.Integer,primary_key=True)
    seqid = db.Column(db.String(255))

    def __init__(self,seqid):
        self.seqid = seqid

    def __repr__(self):
        return '<id {}>'.format(self.id)

class Scheduled(db.Model):
    __tablename__ = 'schedules'

    id = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship("Users",backref="schedules")
    doc_id = db.Column(db.Integer, db.ForeignKey('doctors.id'))
    doctor = db.relationship("Doctors",backref="schedules")
    date = db.Column(db.Date)
    time = db.Column(db.Time)
    status = db.Column(db.String(10))

    def __init__(self,user_id,doc_id,date,time,status):
        self.user_id = user_id
        self.doc_id = doc_id
        self.date = date
        self.time = time
        self.status = status

    def __repr__(self):
        return '<id {}>'.format(self.id)
