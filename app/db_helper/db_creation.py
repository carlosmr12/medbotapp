import psycopg2

import db_helper

con = None
try:
    con = psycopg2.connect(host=db_helper.DB_HOST,database=db_helper.DB_NAME,user=db_helper.DB_USER,password=db_helper.DB_PASSWORD)
    cur = con.cursor()
    query = open('tables.txt','r').read()
    cur.execute(query)
except psycopg2.DatabaseError, e:
    print 'Error {}'.format(e)
finally:
    if con:
        con.close()
