# -*- coding: utf-8 -*-
import csv
import psycopg2

import db_helper as db_helper

con = None
try:
    con = psycopg2.connect(host=db_helper.DB_HOST,database=db_helper.DB_NAME,user=db_helper.DB_USER,password=db_helper.DB_PASSWORD)
    cur = con.cursor()
    with open('fixtures/fakenames.csv','rb') as f:
        reader = csv.reader(f, delimiter=',')
        count = 1
        aux = 1
        for row in reader:
            if aux == 6:
                count += 1
                aux = 1
            insert_query = "INSERT INTO doctors (name,medicine_id,address) VALUES ('{}','{}','Rua Cecília Maria Vallardi Saraceni, 1028')".format(row[0],count)
            cur.execute(insert_query)
            aux += 1
    con.commit()
except psycopg2.DatabaseError, e:
    print 'Error {}'.format(e)
finally:
    if con:
        con.close()
