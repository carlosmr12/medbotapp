# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
import urlparse
import os

urlparse.uses_netloc.append("postgres")
url = urlparse.urlparse(os.environ["DATABASE_URL"])
DB_HOST = url.hostname
DB_NAME = url.path[1:]
DB_USER = url.username
DB_PASSWORD = url.password

def make_connection():
    """
        Helper function to handle connection to database
    """
    con = psycopg2.connect(host=DB_HOST,database=DB_NAME,user=DB_USER,password=DB_PASSWORD)
    return con

def get_user(user_id):
    """
        Helper function to get user from database
    """
    con = make_connection()
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    select_query = "SELECT * FROM users WHERE user_id='{}'".format(user_id)
    cur.execute(select_query)
    response = cur.fetchone()
    con.close()
    return response

def set_user(user_id,first_name,last_name,last_message=str()):
    """
        Helper function to set user in database
    """
    con = make_connection()
    cur = con.cursor()
    insert_query = "INSERT INTO users (user_id,first_name,last_name,last_message) VALUES ('%s','%s','%s','%s')"%(user_id,first_name,last_name,last_message)
    cur.execute(insert_query)
    con.commit()
    con.close()
    return get_user(user_id)

def set_last_message(user_id,last_message=str()):
    """
        Helper function to set user last message in database
    """
    con = make_connection()
    cur = con.cursor()
    update_query = "UPDATE users SET last_message='{}' WHERE user_id='{}'".format(last_message,user_id)
    cur.execute(update_query)
    con.commit()
    con.close()
    return get_user(user_id)

def is_message_processed(seqid):
    """
        Helper function to get processed message by seqid
    """
    con = make_connection()
    cur = con.cursor()
    select_query = "SELECT * FROM processed_messages WHERE seqid='{}'".format(seqid)
    cur.execute(select_query)
    response = cur.fetchone()
    con.close()
    if response is None:
        return False
    return True

def set_message_processed(seqid):
    """
        Helper function to set processed message in database
    """
    con = make_connection()
    cur = con.cursor()
    insert_query = "INSERT INTO processed_messages (seqid) VALUES ('{}')".format(seqid)
    cur.execute(insert_query)
    con.commit()
    con.close()
    return True

def get_doctors_by_medicine(medicine):
    """
        Helper function to get doctors by its medicine from database
    """
    con = make_connection()
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    select_query = "SELECT d.* FROM doctors d INNER JOIN medicines m ON d.medicine=m.id WHERE m.name='%s' LIMIT 5"%(medicine)
    cur.execute(select_query)
    response = cur.fetchall()
    con.close()
    return response

def get_doctor_by_id(doc_id):
    """
        Helper function to get doctor by its id from database
    """
    con = make_connection()
    cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
    select_query = "SELECT * FROM doctors d WHERE id={}".format(doc_id)
    cur.execute(select_query)
    response = cur.fetchone()
    con.close()
    return response
