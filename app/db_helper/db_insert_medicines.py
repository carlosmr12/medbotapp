import csv
import psycopg2

import db_helper as db_helper

con = None
try:
    con = psycopg2.connect(host=db_helper.DB_HOST,database=db_helper.DB_NAME,user=db_helper.DB_USER,password=db_helper.DB_PASSWORD)
    cur = con.cursor()
    with open('fixtures/medicines-apiai.csv','rb') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            insert_query = "INSERT INTO medicines (name) VALUES ('{}')".format(row[0])
            cur.execute(insert_query)
    con.commit()
except psycopg2.DatabaseError, e:
    print 'Error {}'.format(e)
finally:
    if con:
        con.close()
