from flask import Flask
from flask_mail import Mail

app = Flask(__name__)

app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME = 'medbotapp@gmail.com',
    MAIL_PASSWORD = 'UofACH!@3'
    )

from flask_sqlalchemy import SQLAlchemy
import os

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from app import views
