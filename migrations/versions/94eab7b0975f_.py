"""empty message

Revision ID: 94eab7b0975f
Revises: 04396e825bdf
Create Date: 2016-12-07 09:36:48.799037

"""

# revision identifiers, used by Alembic.
revision = '94eab7b0975f'
down_revision = '04396e825bdf'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('processed_messages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('seqid', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('processed_messages')
    # ### end Alembic commands ###
